const validator = require("validator");
const mongoose = require("mongoose");
const { user } = require("../../models");

exports.registerValidators = async (req, res, next) => {
  try {
    const errors = [];
    const { email } = req.body;

    if (!validator.isEmail(email)) {
      errors.push("Please input email correctly");
    }

    let checkEmail = await user.findOne({ email });

    if (checkEmail) {
      errors.push("email already registered");
    }

    if (req.body.password !== req.body.confirm_password) {
      errors.push("Password doesn't matcht with confirm password");
    }

    if (
      validator.isEmpty(req.body.name, {
        ignore_Whitespace: true,
      })
    ) {
      errors.push(`Please input your name`);
    }

    if (
      validator.isEmpty(req.body.address, {
        ignore_Whitespace: true,
      })
    ) {
      errors.push(`Please input your address`);
    }

    if (!mongoose.Types.ObjectId.isValid(req.body.cityId)) {
      errors.push("City is not exist");
    }

    if (
      validator.isEmpty(req.body.hobbies[0], {
        ignore_Whitespace: true,
      })
    ) {
      errors.push(`Please input your hobbies`);
    }

    req.body.last_login = new Date();

    if (errors.length > 0) {
      return next({ statusCode: 403, message: errors });
    }

    next();
  } catch (error) {
    next(error);
  }
};
