const express = require("express");

const authentication = require("../middlewares/auth/auth");
const { registerValidators } = require("../middlewares/validators/user");
const {
  createCity,
  getAllCity,
  register,
  login,
  getAllAccount,
  getAccountById,
  changePassword,
  deleteAccount,
} = require("../controllers/user");

const router = express.Router();

router.post("/city", createCity);
router.get("/city", getAllCity);
router.post("/register", registerValidators, register);
router.post("/login", login);
router.get("/users", getAllAccount);
router.patch("/changePwd", authentication, changePassword);
router.get("/users/:id", getAccountById);
router.delete("/delete/:id", deleteAccount);

module.exports = router;
