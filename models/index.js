require("dotenv").config({ path: `.env.${process.env.NODE_ENV}` });
const mongoose = require("mongoose");

const uri = process.env.MONGO_URI;

mongoose
  .connect(uri)
  .then(() => console.log("Connect to mongoDB"))
  .catch((err) => console.log(err));

exports.user = require("./user");
exports.city = require("./city");
