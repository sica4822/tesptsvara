const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");
require("dotenv").config({ path: `.env.${process.env.NODE_ENV}` });

const UserSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    profile: {
      name: { type: String },
      address: { type: String },
      cityId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "city",
      },
      hobbies: {
        type: Array,
      },
    },
    last_login: {
      type: Date,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

UserSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("user", UserSchema);
