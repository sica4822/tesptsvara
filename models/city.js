const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");
require("dotenv").config({ path: `.env.${process.env.NODE_ENV}` });

const CitySchema = new mongoose.Schema(
  {
    city: {
      type: String,
    },
  },
  {
    timestamps: {
      createdAt: "createdAt",
      updatedAt: "updatedAt",
    },
  }
);

CitySchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("city", CitySchema);
