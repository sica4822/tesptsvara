const { user, city } = require("../models");
const { generateToken } = require("../utils/jwt");

class User {
  async createCity(req, res, next) {
    try {
      await city.create(req.body);

      return res.status(201).json({ message: "city was created" });
    } catch (error) {
      next(error);
    }
  }

  async getAllCity(req, res, next) {
    try {
      let data = await city.find().select("_id city");

      if (data.length === 0) {
        return next({ message: "City not found", statusCode: 404 });
      }

      return res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async register(req, res, next) {
    try {
      const { email, password, name, address, cityId, hobbies, last_login } =
        req.body;
      await user.create({
        email,
        password,
        profile: {
          name,
          address,
          cityId,
          hobbies,
        },
        last_login,
      });

      return res.status(201).json({ message: "Your account has been created" });
    } catch (error) {
      next(error);
    }
  }

  async login(req, res, next) {
    try {
      const { email, password } = req.body;

      let data = await user.findOne({ email });

      if (data.password !== password) {
        return res.status(401).json({
          message: `something wrong with the credentials`,
        });
      }

      let loginDate = new Date();

      data = await user
        .findOneAndUpdate({ email }, { last_login: loginDate }, { new: true })
        .select("-password -deleted")
        .populate({
          path: "profile.cityId",
          select: "city",
          strictPopulate: false,
        });

      const payload = { data };
      const token = generateToken(payload);
      return res.status(200).json({ data, token });
    } catch (error) {
      next(error);
    }
  }

  async getAllAccount(req, res, next) {
    try {
      var perPage = 3,
        page = Math.max(0, req.query.page) || 1;

      let data = await user
        .find()
        .select("-password -profile.address -profile.hobbies -deleted")
        .populate({
          path: "profile.cityId",
          select: "city",
          strictPopulate: false,
        })
        .limit(perPage)
        .skip(perPage * (page - 1))
        .sort({ createdAt: "asc" });

      let count = await user.count();

      if (data.length === 0) {
        return next({ message: "users not found", statusCode: 404 });
      }

      return res
        .status(200)
        .json({ data, page, totalPages: Math.round(count / perPage) });
    } catch (error) {
      next(error);
    }
  }

  async getAccountById(req, res, next) {
    try {
      let data = await user
        .findOne({ _id: req.params.id })
        .select("-password -deleted")
        .populate({
          path: "profile.cityId",
          select: "city",
          strictPopulate: false,
        });

      if (!data) {
        return next({ message: " userId not found", statusCode: 404 });
      }
      delete data.password;
      return res.status(200).json({ data });
    } catch (error) {
      next(error);
    }
  }

  async changePassword(req, res, next) {
    try {
      const { current_password, new_password, confirm_password } = req.body;
      let check = await user.findOne({ _id: req.loginUser.data._id });

      if (check.password !== current_password) {
        return res.status(403).json({ message: `Current Password is wrong!` });
      }

      if (new_password !== confirm_password) {
        return res
          .status(403)
          .json({ message: `Password and Confirm Password didn't match` });
      }

      await user.findOneAndUpdate(
        { _id: req.loginUser.data._id },
        { password: new_password },
        {
          new: true,
        }
      );

      return res.status(201).json({ message: "Password has been changed" });
    } catch (error) {
      next(error);
    }
  }

  async deleteAccount(req, res, next) {
    try {
      let data = await user.findOne({ _id: req.params.id });

      if (!data) {
        return next({ message: " userId not found", statusCode: 404 });
      }

      await user.delete({ _id: req.params.id });

      res.status(201).json({ message: "Account has been deleted" });
    } catch (error) {
      next(error);
    }
  }
}

module.exports = new User();
